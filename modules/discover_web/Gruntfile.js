module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        assemble: {
            options: {
                assets: '../public',
                data: ['data/*.json'],
                partials: 'templates/modules/*.hbs',
                ext: '.html',
                helpers: 'templates/helpers/*.js',
                layout: 'templates/layout/master.hbs',
                removeHbsWhitespace: true
            },
            pages: {
                options: {
                    production: false
                },
                files: [{
                    expand: true,
                    cwd: 'templates/pages',
                    src: ['*.hbs'],
                    dest: 'public'
                }]
            }
        },

        concat: {
            dist: {
                src: ['src/js/core.js','src/js/onReady.js','src/js/ajax.js','src/js/edit_pane.js',
                      'src/js/topics.js'],
                dest: 'public/js/app.js',
            },
        },

        webdriver: {
            test: {
                configFile: './test/wdio.conf.js'
            }
        },

        copy: {
          main: {
            files: [
                {expand: true, cwd: 'src/', src: ['imgs/*'], dest: 'public/'},
                {expand: true, cwd: 'src/', src: ['fonts/*'], dest: 'public/'},
                {expand: true, cwd: 'src/', src: ['libs/*'], dest: 'public/'},
                {expand: true, cwd: 'src/', src: ['index.html'], dest: 'public/'},
            ],
          },
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                'public/css/main.css': 'src/scss/main.scss'
                }
            }
        },

        connect: {
            dev: {
                options: {
                    port: 3000,
                    base: "public",
                    hostname: 'localhost'
                }
            }
        },

        watch: {
            livereload: {
                options: {
                    livereload: true,
                    host: 'localhost',
                    port: 3000
                },
                files: ['public/**/*'],
                },
            scripts: {
                files: ['src/js/**/*.js'],
                tasks: ['concat'],
                options: {
                    spawn: false
                }
            },
           // markup: {
           //     files: ['templates/**/*.hbs','data/*'],
           //     tasks: ['assemble'],
           //     options: {
           //         spawn: false
           //    }
           //  },
            html: {
                files: ['src/*.html'],
                tasks: ['copy'],
                options: {
                    spawn: false,
                },
            },
            css: {
                files: ['src/scss/**/*.scss'],
                tasks: ['sass'],
                options: {
                    spawn: false,
                },
            },
        }, 

    });

    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.registerTask('default', ['concat','copy','sass','watch']);
    grunt.registerTask('dev', ['concat','copy','sass','watch']);
    grunt.registerTask('test', ['concat','copy','sass','webdriver','watch']);
    grunt.registerTask('dist', ['concat','copy','sass']);

};
