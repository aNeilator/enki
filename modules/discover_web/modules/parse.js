var request = require('request');
var config = require('./config')
var parse_endpoint = "https://api.parse.com/1/classes/Content/";

exports.retrieve_content = function(req, res){

	console.log(req.query.request);

	var entries = req.query.entries;
	var skip = req.query.skip;

	var options = {
		url: config.content_endpoint,
		method: 'GET',
		headers: config.headers,
		qs: {'limit' : entries, 'skip' : skip, 'order' : '-createdAt'},
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			var data = JSON.parse(body);
		    res.send(data);
		} 
	});
	
};

exports.approve_content = function(req, res){

	console.log(req.query.request);

	var date = new Date();
	var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

	var objectId = req.query.objectId;

	var options = {
		url: config.content_endpoint + objectId,
		method: 'PUT',
		headers: config.headers,
		json: {
			'approved' : true,
			'status' : 'approved',
			'curation' : {'date': Date() , 'name': 'admin'}
		},
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
		    console.log(response.body);
		    res.send('Approved!');
		} 
	});

};

exports.reject_content = function(req, res){

	console.log(req.query.request);

	var date = new Date();
	var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

	var objectId = req.query.objectId;

	var options = {
		url: config.content_endpoint + objectId,
		method: 'PUT',
		headers: config.headers,
		json: {
			'approved' : false,
			'status' : 'rejected',
			'curation' : {'date': Date() , 'name': 'admin'}
		},
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
		    console.log(response.body);
		    res.send('Rejected!');
		} 
	});

};

exports.delete_content = function(req, res){

	console.log(req.query.request);

	var objectId = req.query.objectId;

	var options = {
		url: config.content_endpoint + objectId,
		method: 'DELETE',
		headers: config.headers,
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
		    res.send('Deleted!');
		} 
	});

};

exports.like_content = function(req, res){

	console.log(req.query.request);
	
	var likes = parseInt(req.query.likes);
	var objectId = req.query.objectId;

	var options = {
		url: config.content_endpoint + objectId,
		method: 'PUT',
		headers: config.headers,
		json: {'likes' : likes},
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log(response.body);
			res.send('Liked!');
		} 
	});

};

exports.keyword_search = function(req, res){

	console.log(req.query.request);

	var keyword = req.query.keyword;

	console.log(keyword);

	var options = {
		url: config.content_endpoint,
		method: 'GET',
		headers: config.headers,
		qs: {'order' : '-createdAt', 'where': {'tags': keyword}},
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			var data = JSON.parse(body);
		    res.send(data);
		} 
	});

};

exports.update_content = function(req, res){

	console.log(req.query.request);

	var date = new Date();
	var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

	var objectId = req.query.objectId;
	var description = req.query.description;
	var tags = req.query.tags;
	var imageUrl = req.query.imageUrl;

	var options = {
		url: config.content_endpoint + objectId,
		method: 'PUT',
		headers: config.headers,
		json: {
			'tags' : tags,
			'description' : description,
			'imageUrl' : imageUrl,
			'curation' : {'date': Date() , 'name': 'admin'}
		},
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
		    console.log(response.body);
		    res.send('Updated!');
		} 
	});

};
