var request = require('request');
var config = require('./config')

exports.retrieve_sources = function(req, res){

	var entries = req.query.entries;
	var skip = req.query.skip;

	var options = {
		url: config.sources_endpoint,
		method: 'GET',
		headers: config.headers
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			var data = JSON.parse(body);
		    res.send(data);
		} 
	});

};

exports.add_sources = function(req, res){

	var source_name = req.query.source_name;

	var options = {
		url: config.sources_endpoint,
		method: 'POST',
		headers: config.headers,
		json: {'source': source_name},
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 201) {
		    console.log(body);
		} 
	});

};