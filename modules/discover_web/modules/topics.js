var request = require('request');
var config = require('./config')

exports.retrieve_topics= function(req, res){

	var entries = req.query.entries;
	var skip = req.query.skip;

	var options = {
		url: config.topics_endpoint,
		method: 'GET',
		headers: config.headers
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			var data = JSON.parse(body);
		    res.send(data);
		} 
	});

};

exports.add_topics = function(req, res){

	var classifier_name = req.query.classifier_name;

	var options = {
		url: config.topics_endpoint,
		method: 'POST',
		headers: config.headers,
		json: {'classifier': classifier_name},
	};

	request(options, function (error, response, body) {
		if (!error && response.statusCode == 201) {
		    console.log(body);
		} 
	});

};