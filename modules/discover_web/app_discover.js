'use strict'

var bodyParser = require('body-parser');
var express = require('express');
var request = require('request');
var parse = require('./modules/parse');
var sources = require('./modules/sources');
var topics = require('./modules/topics');

var app = express();
var port = '8080';

app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

app.get('/parse/content', parse.retrieve_content);
app.get('/parse/approval', parse.approve_content);
app.get('/parse/reject', parse.reject_content);
app.get('/parse/delete', parse.delete_content);
app.get('/parse/like', parse.like_content);
app.get('/parse/update', parse.update_content);
app.get('/parse/keyword', parse.keyword_search);

app.get('/source/list', sources.retrieve_sources);
app.get('/source/add', sources.add_sources);

app.get('/topics/list', topics.retrieve_topics);
app.get('/topics/add', topics.add_topics);

app.listen(port, function() {
    console.log('Discover Web listening :: ', port);
});
