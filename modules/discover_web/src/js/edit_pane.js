myPage.edit_pane = (function(){

	var ID = "";

	var showPane = function(contentID){

			ID = contentID;

			$('body').addClass('stop_scrolling')

			var tags = [];
			var description = $('#'+ID).find('.tweet_description').text();
			var header = $('#'+ID).find('.tweet_header').text();
			var url = $('#'+ID).find('.image img').attr('src');

			$('#'+ID).find('.keyword').each(function(){
				tag = $(this).text();
				tags.push(" " + tag + " ");
			});

			$('.form_header').html(header);
			$('.description').val(description);
			$('.tags').val(tags);
			$('.url').val(url);

			$('.edit').css({'visibility': 'visible'});
			$('.entry_form').css({'opacity': 1});
	};

		submit = function(){

		if (confirm("Submit changes?")) {

			var new_div = "";
			var tag = "";
			var newTags = [];

			var newDescription = $('.description').val();
			var newUrl = $('.url').val();
			var tags = $('.tags').val().split(',');

			for (var i = 0; i < tags.length; i++){
				tag = tags[i]
				tag = tag.toLowerCase();
				newTags.push(tag.trim());
			}

			$.ajax({
				type: "GET",
				url: "/parse/update",
				data : {
					"request": "content_request",
					"objectId": ID,
					"description": newDescription,
					"tags": newTags,
					"imageUrl": newUrl
				},
				success : function(data) {
					console.log(data);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					hideWheel()
					$("#content-holder").html("<h2 class='error'>Error loading Content</h2>");
				}
			});

			$("#"+ID ).find(".tweet_description").html("<p>"+newDescription+"</p>");
			$('#'+ID).find('.image img').attr('src', newUrl);
			$('#'+ID).find('.keywords').empty();

			for (var i = 0; i < newTags.length; i++) {
				new_div = "<div class='keyword'><h3 onclick='keywordSearch(this)'>"+newTags[i]+"</h3></div>"
				$('#'+ID).find('.keywords').append(new_div);
				newTags[i];
			}

			closePane();

		} else {
			closePane();
		};
	};

		closePane = function(){
			$('.entry_form').css({'opacity': 0});
			$('body').removeClass('stop_scrolling')
			$('.edit').css({'visibility': 'hidden'});
	};

		init = function(){
			$('.submit_button').click(submit)
			$('.close_cross').click(closePane)
			$('.cancel_button').click(closePane);
	};

	return { 
		init: init,
		showPane: showPane,
		closePane: closePane
	}

})();