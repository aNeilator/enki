$(document).ready(function() {

	myPage.ajax.init();
	myPage.edit_pane.init();
	myPage.topics.init();

	$(document).on({
	    mouseenter: function () {
	    	$(this).html('<p>Undo</p>');
	    },
	    mouseleave: function () {
	    	$(this).html('<p>Approved</p>');
	    }
	}, '.approved');

});