myPage.ajax = (function(){

	var skip = 0;

		contentRequest = function(entries){

			$.ajax({
				type: "GET",
				url: "/parse/content",
				data : {
					"request": "content_request",
					"entries": entries,
					"skip": skip
				},
				success : function(data) {
					templateContent(data);
			    	$('.load_more').show();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					hideWheel();
					$("#content-holder").html("<h2 class='error'>Error loading Content</h2>");
				}
			});

			skip += entries;

	};
	
		approval = function(contentID){

			hideTweet(contentID);

			var approval_button = $('#'+contentID).find('.approval_button')

			$.ajax({
				type: "GET",
				url: "/parse/approval",
				data : {
					"request": "approval_request",
					"objectId": contentID,
				},
				success : function(data) {
					showTweet(contentID);

					approval_button.attr("onclick","undo('"+ contentID + "')");
					approval_button.html('<p>Approved</p>');
					approval_button.addClass('approved');
					approval_button.hover(
						function() {
							$( this ).html($( '<p>Undo</p>' ));
						}, function() {
							$( this ).html($( '<p>Approved</p>' ));
						}
					);
				
				}
			});

	};

		undo = function(contentID){

			hideTweet(contentID);

			var undo_button = $("#"+contentID).find(".approval_button")

			$.ajax({
				type: "GET",
				url: "/parse/reject",
				data : {
					"request": "reject_request",
					"objectId": contentID,
				},
				success : function(data) {
					showTweet(contentID);
					undo_button.attr("onclick","approval('"+ contentID + "')");
					undo_button.html('<p>Approve</p>');
					undo_button.removeClass('approved');
					undo_button.hover( 
						function() {
							$( this ).html($( '<p>Approve</p>' ));
						}, function() {
							$( this ).html($( '<p>Approve</p>' ));
						}
					);
				}
			});
	};


		like = function(contentID){

			var likes = $("#"+contentID).find(".likes");
			var likes_int = parseInt(likes.text()) + 1;

			$.ajax({
				type: "GET",
				url: "/parse/like",
				data : {
					"request": "like_request",
					"objectId": contentID,
					"likes": likes_int
				},
				success : function(data) {
					likes.text(likes_int);
					console.log(data);
				}
			});
	};

		deletePost = function(contentID){

		    if (confirm("Are you sure you want to delete this content?")) {
		    
				hideTweet(contentID);

				$.ajax({
					type: "GET",
					url: "/parse/delete",
					data : {
						"request": "delete_request",
						"objectId": contentID,
					},
					success : function(data) {
						console.log(data);
						contentRequest(1);
						$('#'+contentID).hide()
					}
				});
		    }
	};

		keywordSearch = function(keyword_html){

			var keyword = $(keyword_html).text();

			$('.load_more').hide();
			$('#content-holder').hide();
	    	showWheel();

			$.ajax({
				type: "GET",
				url: "/parse/keyword",
				data : {
					"request": "keyword_search_request",
					"keyword": keyword
				},
				success : function(data) {
					$('.keyword_holder').css({'opacity': 0});

					var source = $("#content-template").html();
					var template = Handlebars.compile(source);
					$("#content-holder").html(template(data));

					$(".separator").show(1000);
					$(".search").show();
					$(".search").text(keyword);

			    	hideWheel();
					$('#content-holder').show();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
			    	hideWheel();
					$("#content-holder").html("<h2 class='error'>Error loading Content</h2>");
				}
			});

	};

		reload = function(){

			var entries = 9;
			skip = 0;

			$.ajax({
				type: "GET",
				url: "/parse/content",
				data : {
					"request": "content_request",
					"entries": entries,
					"skip": skip
				},
				success : function(data) {
					var source = $("#content-template").html();
					var template = Handlebars.compile(source);
					$("#content-holder").html(template(data));

					$(".separator").hide(1000);
					$(".search").hide();
			    	$('.load_more').show();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					hideWheel()
					$("#content-holder").html("<h2 class='error'>Error loading Content</h2>");
				}
			});

			skip += entries;

	};

		templateContent = function(data){
			var source = $("#content-template").html();
			var template = Handlebars.compile(source);
			$("#content-holder").append(template(data));
	    	hideWheel()
	};

		loadMore = function(){
			contentRequest(9);
	};

		showWheel = function(){
			$(".load_wheel").show();
	};

		hideWheel = function(){
			$(".load_wheel").hide();
	};

		hideTweet = function(contentID){
			$('#'+contentID).find('.cover_tweet').css({'visibility': 'visible'})
			$('#'+contentID).find('.cover_tweet').css({'opacity': 0.8})
	};

		showTweet = function(contentID){
			$('#'+contentID).find('.cover_tweet').css({'visibility': 'hidden'})		
			$('#'+contentID).find('.cover_tweet').css({'opacity': 0})
	};

		init = function(){
			$(document).ready(contentRequest(9));
			$(".load_more").click(loadMore);
			$(".reload").click(reload);
			$(".tweet").click(approval);

	};

	return { 
		init: init, 
		approval: approval,
		like: like,
		deletePost: deletePost,
		keywordSearch: keywordSearch,
		reload: reload
	}

})();