myPage.topics = (function(){

	var skip = 0;

		topicsRequest = function(){

			$.ajax({
				type: "GET",
				url: "/topics/list",
				success : function(topic_data) {
					templateTopics(topic_data);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					hideWheel();
					$("#content-holder").html("<h2 class='error'>Error loading Content</h2>");
				}
			});
	};

		templateTopics = function(topic_data){
			var source = $("#topics-template").html();
			var template = Handlebars.compile(source);
			$("#topics-holder").append(template(topic_data));
	};

		showTopics = function(){

			$(".topics-box").slideToggle('medium', function() {
			    if ($(this).is(':visible')) {			    	
			        $(this).css('display','inline-block');
			    };
			});

			if ($(".topics-header").hasClass("selected")){
				$(".topics-header").removeClass("selected");
			} else {
				console.log("Hi")
				$(".topics-header").addClass("selected");
			}


	};

		init = function(){
			$(document).ready(topicsRequest());
			$(".channel_button").click(showTopics);
	};

	return { 
		init: init
	}

})();