var f_modules = 'modules/'

try {
    var classifier_tester = require('./classifier/app_classifier.js');
} catch (e) {
    console.log(" XXX ERROR Occured in CLASSIFER\n" + e);
}

try {
    var listenTwitter = require('./twitter/app_twitter.js');
} catch (e) {
    console.log(" XXX ERROR Occured in TWITTER\n" + e);
}

try {
    var listenEmail = require('./email/app_email.js');
} catch (e) {
    console.log(" XXX ERROR Occured in EMAIL\n" + e);
}

try {
    var listenEmail = require('./slack/app_slack.js');
} catch (e) {
    console.log(" XXX ERROR Occured in SLACK\n" + e);
}

try {
    var launchCurationPage = require('./discover_web/app_discover.js');
} catch (e) {
    console.log(" XXX ERROR Occured in DISCOVER\n" + e);
}

try {
    var launchCurationPage = require('./parse/app_parse.js');
} catch (e) {
    console.log(" XXX ERROR Occured in PARSE\n" + e);
}