''' Twitter Scrapper '''

from twython import Twython
from difflib import SequenceMatcher
from pymongo import MongoClient

import time
import progressbar
import json
import sys
import requests 
import datetime
import pyprind

reload(sys)  
sys.setdefaultencoding('utf8')

def connect_twitter():

    ''' Connects to Twitter '''

    twitter = Twython("F3nUOgWWOBt5FzaCrmNiWRYSE",
                      "WzrJkba3AtFdi0YTOuHfsaSTFTdjqMPCdyT6jcJN4UWIlLX9Wu",
                      "3294028125-1I9zRvKKUlJMTXYhnS8dpcngnTsW9O6yU6xASHq",
                      "HvkGN7WGYGos3kh9UUrCNpG0CbuxCubnuryZr0klj4fYB")

    return twitter

def get_channels(channels_db):

    '''Gets sources from Parse'''

    channels = []

    for entry in channels_db.find():
        channels.append(entry['channel'])

    return channels

def get_keywords(keywords_db):

    '''Gets sources from Parse'''

    keywords = []

    for entry in keywords_db.find():
        keywords.append(entry['keyword'])

    return keywords

def get_tweets(tweets_db):

    tweets = []

    for entry in tweets_db.find():
        tweets.append(entry['description'])

    return tweets

def accept_reject(text, check_duplicates, tweets_db):

    if "RT " not in text and "https://" in text:
        if text not in check_duplicates:
            if process_text(text) not in tweets_db:
                return True

    return False

def check_for_keyword(text, keyword):

    ''' Check if keyword is in tweet '''

    return " " + keyword.lower() + " " in text.lower()

def get_link(text):

    for entry in text.split():
        if 'https' in entry: 
            return entry

def process_text(text):

    ''' Remove links and hashtag from raw tweet '''

    text_list = text.split()

    if ':' in text_list:
        text_list.remove(':')
    elif '|' in text_list:
        text_list.remove('|')

    text_no_links = str()

    for word in text_list:
        if "http" in word:
            pass
        elif "#" in word:
            pass
        elif "@" in word:
            pass
        else: 
            text_no_links += word + " "

    return text_no_links

def db_format(tweet, keyword):

    ''' Reformat for Parse DB ''' 

    article_link = get_link(tweet['text'])

    classifier = classify(tweet["text"], article_link)

    tags = get_tags(classifier)
    image = get_image(classifier)

    tags.insert(0,keyword)

    parse_tweet = {
        "title": tweet["user"]["name"],
        "description": process_text(tweet['text']),
        "date": tweet["created_at"],
        "status": "pending",
        "source": "Twitter",
        "url": article_link,
        "tags": tags,
        "imageUrl": image,
        "type": "twitter",
        "approved": False,
        "curation": {"date":"","name": ""},
        "likes": 0,
        "tweetID": tweet["id"]
    }

    return parse_tweet

def classify(text, url):

    '''Get tags and image from embedly'''

    start = time.time()

    payload = {
        'key': '1197d575613b4c10bd1f50d8330bb2db',
        'url': url
    }

    r = requests.get('https://api.embedly.com/1/extract', params=payload)

    finish = str(round(time.time() - start))

    return json.loads(r.text)

def get_tags(classifier): 

    tags = []

    keywords = classifier['keywords'][:6]

    for tag in keywords:
        tags.append(tag['name'])

    return tags

def get_image(classifier):

    try:
        classifier['images'][0]['url']
    except IndexError:
        return ""
    else:
        return classifier['images'][0]['url']

def send_to_mongo(tweet_db, data):

    if len(data) == 0:
        print '\n Nothing to send!'
    else:
        for entry in pyprind.prog_bar(data):
            del entry['tweetID']
            tweet_db.insert_one(entry)

def sort_by_date(tweets):

    '''Sorts tweets by id'''

    start = time.time()

    swapped = True

    while swapped:
        swapped = False
        for i in range(len(tweets)-1):
            if tweets[i]['tweetID'] > tweets[i+1]['tweetID']:
                temp = tweets[i]
                tweets[i] = tweets[i+1]
                tweets[i+1] = temp
                swapped = True

    finish = str(time.time() - start)
    print "- Sorted tweets in " + finish + " secs"

    return tweets

def check_similarity(tweets, text_list):

    ''' Uses sequence matcher to check string similarity '''

    start = time.time()

    checked = []
    too_similar = []

    for string_a in text_list:
        checked.append(string_a)
        for string_b in text_list:
            if string_b in checked:
                pass
            elif  1.0 > SequenceMatcher(None, string_a, string_b).ratio() >= 0.7:
                too_similar.append(string_b)

    initial_tweets = len(tweets)

    for remove in too_similar:
        for i in range(len(tweets)-1):
            if remove == tweets[i]['description']:
                del tweets[i]

    finish = str(time.time() - start)
    final_tweets = str(initial_tweets - len(tweets))
    print "\n- Removed " + final_tweets + " tweets in " + finish + " secs"

    return tweets

def main():

    '''Main Search for Relavent Tweets'''

    client = MongoClient('mongodb://localhost:27017/')
    db = client['LOI']

    twitter = connect_twitter()

    tweets_db = get_tweets(db['tweets'])
    channels_db = get_channels(db['channels'])
    keywords_db = get_keywords(db['keywords'])

    tweets_json = []
    check_duplicates = []

    start = time.time()

    print "\nScrapping Some Great Content!\n"

    for channel in channels_db:
        tweets = twitter.get_user_timeline(screen_name=channel, count=200)
        print '- Searching ' + channel
        for tweet in pyprind.prog_bar(tweets):
            for keyword in keywords_db:
                if check_for_keyword(tweet['text'], keyword):
                    if accept_reject(tweet['text'], check_duplicates, tweets_db):
                        check_duplicates.append(str(tweet['text']))
                        tweets_json.append(db_format(tweet, keyword))

    tweets_json = check_similarity(tweets_json, check_duplicates)
    tweets_json = sort_by_date(tweets_json)
   
    send_to_mongo(db['tweets'], tweets_json)

    finish = str(round(time.time() - start))
    print "- Found " + str(len(tweets_json)) + " tweets in " + finish + " secs"

if __name__ == '__main__':
    main()
