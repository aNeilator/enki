// --- SETTING UP PARSE ---
var Parse = require('parse/node');
Parse.initialize("TGarbNbK628kcTuKuxx74hQSdHkLAkF6vg5i2U4c", "WwyI0ipnsOowxtK0ScxFbTsqPstDsyFuG4dUu7Ox");

// *************
// --- USERS ---
// *************

// --- USER LOGIN ---

exports.userLogin = function(username, password) {

    Parse.User.logIn(username, password, {
        success: function(user) {
            console.log("---");
            console.log("START");
            console.log("---");
            console.log("User logged in successfully (" + user.get("username") + ").");
            console.log("---");
            console.log("END");
            console.log("---");
        },
        error: function(user, error) {

        }
    });
}

// --- GET CURRENT USER ---

exports.currentUser = function() {

    var currentUser = Parse.User.current();
    if (currentUser) {
        console.log("---");
        console.log("START");
        console.log("---");
        console.log("Current user: " + currentUser);
        console.log("---");
        console.log("END");
        console.log("---");
    } else {
        console.log("---");
        console.log("START");
        console.log("---");
        console.log("No current user.");
        console.log("---");
        console.log("END");
        console.log("---");
    }
}

// --- 'LIKE' AN ENTRY ---



// *************
// --- DATA ---
// *************

// --- RECEIVE JSON AND STORE DATA FROM EXTERNAL AND INTERNAL SOURCES ---

exports.saveEntryToParse = function(jsonObject, clas) {
    var Content = Parse.Object.extend(clas);

    var contentObject = new Content();
    contentObject.set("title", jsonObject.title);
    contentObject.set("description", jsonObject.description);
    contentObject.set("source", jsonObject.source);
    contentObject.set("url", jsonObject.url);
    contentObject.set("tags", jsonObject.tags);
    contentObject.set("imageUrl", jsonObject.imageUrl);
    contentObject.set("type", jsonObject.type);
    var today = new Date();
    contentObject.set("date", today);
    contentObject.set("status", "pending");
    contentObject.set("curation", {
        "name": "",
        "date": ""
    });
    contentObject.set("likes", 0);

    // console.log("Saving entry: " + JSON.stringify(contentObject));

    contentObject.save(null, {
        success: function(contentObject) {
    		console.log("PARSE: Successfully uploaded to parse!");
        },
        error: function(contentObject, error) {
            console.log('PARSE: Failed to create new object, with error code: ' + error.message);
        }
    });

}

exports.saveEntriesToParse = function(json) {
    var Content = Parse.Object.extend("Content");
    var jsonObject = new Content();
    var entryCount = 0;

    console.log("---");
    console.log("START");
    console.log("---");
    console.log("Preparing to upload " + json.length + " entries.");

    for (var i = 0; i < json.length; ++i) {
        jsonObject = json[i];
        entryCount++;
        saveEntryToParse(jsonObject);
    }

    console.log("---");
    console.log("Successfully uploaded " + json.length + " entries.");
    console.log("---");
    console.log("END");
    console.log("---");
}

// --- UPDATE CONTENT DURING CURATION --

exports.setStatusForEntry = function(objectId, status, name) {
    var Content = Parse.Object.extend("Content");
    var query = new Parse.Query(Content);

    query.equalTo("objectId", objectId);

    query.find({
        success: function(results) {

            var contentObject = results[0];

            var today = new Date();
            var dict = {
                "name": name,
                "date": today
            };

            contentObject.set("curation", dict);
            contentObject.set("status", status);

            contentObject.save(null, {
                success: function(contentObject) {
                    console.log("---");
                    console.log("START");
                    console.log("---");
                    console.log("Entry updated");
                    console.log("New status: " + status);
                    console.log("Curation data: " + JSON.stringify(dict));
                    console.log("---");
                    console.log("Full entry:");
                    console.log(JSON.stringify(contentObject));
                    console.log("---");
                    console.log("END");
                    console.log("---");
                },
                error: function(contentObject, error) {
                    console.log("---");
                    console.log('Failed to update object, with error code: ' + error.message);
                }
            });
        }
    })
}

// --- EXPORT JSON TO WEB APPLICATION ---

exports.getEntries = function(skip, limit, status) {
    var Content = Parse.Object.extend("Content");
    var query = new Parse.Query(Content);

    if (status === "") {
        query.notEqualTo("objectId", "");
    } else {
        query.equalTo("status", status);
    }

    query.descending("date");
    query.skip(skip);
    query.limit(limit);

    query.find({
        success: function(results) {
            console.log("---");
            console.log("START");
            console.log("---");
            console.log("Query settings:");
            console.log("Skip: " + skip);
            console.log("Limit: " + limit);
            console.log("Status: " + status);
            console.log("---");
            console.log("Getting JSON for curated content...");
            console.log("---");
            console.log("Successfully retrieved " + results.length + " entries.");
            console.log("---");
            for (var i = 0; i < results.length; ++i) {
                var contentObject = results[i];
                console.log(JSON.stringify(contentObject));
                console.log("---");
            }
            console.log("END");
            console.log("---");
        },
        error: function(error) {
            console.log("Error: " + error.code + " " + error.message);
        }
    });
}

// --- GET A SINGLE ENTRY

exports.getEntryWithId = function(objectId) {
    var Content = Parse.Object.extend("Content");
    var query = new Parse.Query(Content);

    query.equalTo("objectId", objectId);

    query.find({
        success: function(results) {
            console.log("---");
            console.log("START");
            console.log("---");
            console.log("Getting entry for objectId: " + objectId);
            console.log("---");
            console.log(JSON.stringify(results[0]));
            console.log("---");
            console.log("END");
            console.log("---");
        },
        error: function(error) {
            console.log("Error: " + error.code + " " + error.message);
        }
    })
}

// --- GET ENTRIES WITH TAGS ---

exports.getEntriesWithTag = function(tags) {
    var Content = Parse.Object.extend("Content");
    var query = new Parse.Query(Content);

    query.containedIn("tags", tags);

    query.find({
        success: function(results) {
            console.log("---");
            console.log("START");
            console.log("---");
            console.log("Getting entry with tag(s): " + tags);
            console.log("---");
            for (var i = 0; i < results.length; ++i) {
                var contentObject = results[i];
                console.log(JSON.stringify(contentObject));
                console.log("---");
            }
            console.log("END");
            console.log("---");
        },
        error: function(error) {
            console.log("Error: " + error.code + " " + error.message);
        }
    })
}