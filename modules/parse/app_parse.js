var express = require('express');
var app = express();
var myJson = require('./content.json');

var parse = require('./parse');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.get('/', function (req, res) {
  res.send('Parse is up and running, fams.');
});

app.post('/', function(req, res) {
    corAnalyser.analyseCorpus(req.body, sendResults, res);
});

var server = app.listen(process.env.PORT || 5000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('PARSE listening at http://%s:%s', host, port);
});

// Run Parse functions
//parse.userLogin("nick.farrant@akqa.com", "1Sunnired89");
//parse.currentUser();
//parse.saveEntriesToParse(myJson);
//parse.setStatusForEntry("n9W47oC5dK","approved","Nick Farrant");
//parse.getEntries(0,100,"pending");
//parse.getEntryWithId("n9W47oC5dK");
//parse.getEntriesWithTag(["Test","Hello","Cunt"]);