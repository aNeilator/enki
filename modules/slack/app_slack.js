var express = require('express');
var path = require('path');
var app = express();
var fs = require('fs');
var https = require('https');
var bodyParser = require('body-parser');
var request = require('request');
var textProcessor = require('./../classifier/analysis_corpus.js');
var parseSender = require('./../parse/parse.js');
var port = process.env.PORT || 4000;
var options = {
    key: fs.readFileSync(path.join(__dirname) + '/ssl/server.key'),
    cert: fs.readFileSync(path.join(__dirname) + '/ssl/server.crt')
};
var forceStr = '<<force>>';
var testStr = '<<test>>';


app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(express.static(__dirname));
// app.use(express.static(__dirname + '/../classifier/public/'));

// https.createServer(options, app).listen(port, function() {
//     console.log("Express server listening on port " + port);
// });


app.get('/', function(req, res) {
    app.use(express.static(__dirname));
    res.writeHead(200, {
        "Content-Type": "text/html"
    });
    console.log("someone arrived!");
    res.write("Slack App is listening\nUse /enki \<description\> \<url\>\nOn slack ");
    res.end();
});

app.post('/', function(req, res) {
    console.log("CALLED SLACK");

    var usageStr = 'Usage:\n`/enki My small description. http://mylink.com`\n' +
        '`<<test>> description http://link.com` for verification only. (not uploaded to Enki App)\n' +
        '`<<force>> description http://link.com` to force verification to Accept link';

    var options = {},
        extract = {},
        info = {};
    // try {
    // console.log("extracted: " + JSON.stringify(extract));
    // console.log("options: " + JSON.stringify(options));
    extract = analyseSlackMsg(req.body);
    options = generateOptions(extract);
    // } catch (e) {
    // extract.formatOK = false;
    // }
    res.end();
    // console.log(extract.formatOK + " FORMAT " +extract.response_url);
    if (!extract.formatOK) {
        // respondError(res, usageStr);
        var body = {
            "response_type": "ephemeral",
            "text": usageStr
        };
        makePOST(extract.response_url, body);
    } else {
        try {
            // res.send('Analysing link... ' + extract.myurl);
            // res.end();
            info = textProcessor.analyseCorpus(options, function(myInfo) {
                if (myInfo.malformed) {
                    var body = {
                        "response_type": "ephemeral",
                        "text": "Malformed URL or server isn't running"
                    };
                    makePOST(extract.response_url, body);
                }
                sendResults(myInfo, extract);
            }, undefined);
            // console.log("info: " + JSON.stringify(info));
            // res.end();
        } catch (e) {
            var body = {
                "response_type": "ephemeral",
                "text": "An Error occurred. 0x123"
            };
            makePOST(extract.response_url, body);
        }
    }

});


//--------CLASSIFIER WEB

app.get('/classifier/', function(req, res) {
    app.use(express.static(__dirname + '/../classifier/public/'));
    res.sendFile(path.join(__dirname + '/../classifier/public/index.html'));
});

app.post('/classifier/', function(req, res) {
    console.log("CALLED CLASSIFIER");
    try {
        textProcessor.analyseCorpus(req.body, 
            function(info,res){
                res.send(info);
        }, res);
        // res.send(info);
    } catch (e) {
        var info = {}
        res.send(info);
    }

    // res.end();
});

//--------CLASSIFIER WEB

app.listen(port, function() {
    console.log('CLASSIFIER WEB listening :: ', port);
});

function respondError(res, msg) {
    console.log(msg);
    res.send(msg);
    res.end();
}

function analyseSlackMsg(result) {

    var descr = result.text;
    result.myurl = "";
    var urls = new Object();
    try {
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        // var urlRegex2 = /\b(http|https)?(:\/\/)?(\S*)\.(\w{2,4})\b/ig;
        urls = (result.text).match(urlRegex);
        urls[0] && (result.myurl = urls[0]);
        urls.forEach(function(x) {
            descr = descr.replace(x, '');
        });
    } catch (e) {
        console.log("Malformed URL");
        result.formatOK = true;
    }

    result.formatOK = true;
    if (urls === null) {
        console.log("No URL included!");
        result.formatOK = false;
    }

    result.forced = false;
    if (descr.match(forceStr)) {
        descr = descr.replace(forceStr, '');
        result.forced = true;
    }
    result.testOnly = false;
    if (descr.match(testStr)) {
        descr = descr.replace(testStr, '');
        result.testOnly = true;
    }

    //clean text. only have descriptions
    result.mydescription = descr;
    return result;
}

function generateOptions(extract) {
    return {
        "corpus": extract.mydescription,
        "url": extract.myurl,
        "tag_config": {
            "total": 20,
            "shuffle": false
        },
        "threshold": 0.5
    };
}

function sendResults(infoResp, extract) {
    var classStr = infoResp.topclass.mclass + ":" + infoResp.topclass.amount;
    console.log("-------------------------------------");
    var prefix = '';
    if (infoResp.relevant || extract.forced == true) {
        if (!extract.testOnly == true) {
            sendToParse(extract, infoResp);
        }
        sendSlackReply(infoResp, extract, true);
    } else {
        sendSlackReply(infoResp, extract, false);
    }
    console.log("-------------------------------------");
}

function sendSlackReply(infoResp, extract, success) {
    var prefix = '';
    if (extract.testOnly) {
        prefix = "TEST ONLY : ";
    }
    var str = prefix;
    if (success) {
        str += "Accepted! CLASS: " + infoResp.topclass.mclass;
    } else {
        if (infoResp.malformed) {
            str += '404! Link is not reachable';
        } else {
            str += 'Rejected! Link is not relevant to new technologies.';
        }
    }
    console.log(str);
    var lb = '<'; //'&lt;';
    var rb = '>'; //'&gt;'

    var cls = 'Enki Analysis for ' + lb + extract.myurl + '|' + infoResp.mainTitle + rb;
    var body = {
        "response_type": "ephemeral",
        "text": cls,
        "attachments": [{
            "text": str
        }]
    };
    makePOST(extract.response_url, body);

}

function makePOST(uri, body) {

    var options = {
        method: 'post',
        body: body, // Javascript object
        json: true, // Use,If you are sending JSON data
        url: uri,
        headers: {
            'Content-Type': 'application/json'
        }
    }

    request(options, function(err, res, bd) {
        if (err) {
            console.log('SLACK: Error :', err);
            return;
        } else {
            console.log("SLACK: Successfully sent back reponse " + body.text);
        }
    });
}

function sendToParse(extract, infoResp) {
    var keywords = [];
    var words = infoResp.keywords;
    for (key in words) {
        // console.log("TAGS: "+words[key][0]);
        keywords.push(words[key][0]);
    }
    var parseJSON = {
        "title": infoResp.mainTitle,
        "description": extract.mydescription,
        "source": infoResp.mainSource,
        "url": extract.myurl,
        "tags": keywords,
        "imageUrl": infoResp.mainImage,
        "type": "slack [" + extract.user_name + "]",
    };
    parseSender.saveEntryToParse(parseJSON, 'Content');
}