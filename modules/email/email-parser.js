var fs = require('fs');
var path = require('path');
var textProcessor = require('./../classifier/analysis_corpus.js');

var MLFile = JSON.parse(fs.readFileSync(
    path.join(__dirname) + '/email_options.json', 'utf8'));
var subjectALLOW = MLFile.email.subject_accept,
    subjectREJECT = MLFile.email.subject_reject;

exports.analyseEmail = function(email) {
    // console.log("EMAIL: "+email);
    var subjectOK = textProcessor.WordsExist(email.Subject, subjectALLOW) === true;
    var rejectedsubjectOK = textProcessor.WordsExist(email.Subject, subjectREJECT) === false;

    var allowEmail = false;
    var infoResp;

    if (rejectedsubjectOK) {

        var urls = textProcessor.extractUrls(email.Body.Content);
        if (urls.length > 0) {
            var provideUrl = "";
            if (urls[0].length > 0) {
                provideUrl = urls[0].toString();
                // console.log(provideUrl);
            }
            var corpusJSON = {
                "classes": "",
                "corpus": email.Body.Content + ' ' + email.Subject,
                "url": provideUrl,
                "tag_config": {
                    "total": 10,
                    "shuffle": false
                },
                "threshold": 0.5
            };

            var bodyOK = false;
            var classStr = '';
            var topclassAmount = 0;
            try {
                var infoResp = textProcessor.analyseCorpus(corpusJSON);
                bodyOK = infoResp.relevant === true;
                classStr = infoResp.topclass.mclass + ":" + infoResp.topclass.amount;
                topclassAmount = infoResp.topclass.amount;
            } catch (e) {}

            allowEmail = subjectOK || bodyOK;

            if (allowEmail && topclassAmount > 0) {
                console.log(classStr + " :: " + email.Subject);
                writeToJSON(corpusJSON, infoResp);
            } else {
                // console.log("---Email not Allowed: " + email.Subject);
                // if (topclassAmount > 0)
                //     console.log("--not quite {" + classStr + "} :: " + email.Subject);
            }
        }else{
            console.log("URL doesnt exist");
        }
    }

    if (infoResp === undefined) {
        infoResp = {
            "topclass": {
                "class": "",
                "amount": 0
            },
            "relevant": false
        };
    }

    return {
        "allow": allowEmail,
        "analysis": infoResp
    }
}


function writeToJSON(corpusJSON, info) {
    // console.log("Writing to JSON...");

}