// Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. See full license at the bottom of this file.
var server = require("./server");
var router = require("./router");
var authHelper = require("./authHelper");
var outlook = require("node-outlook");
var email_analyser = require('./email-parser.js');
var express = require('express');
var path = require('path');
var app = express();
var fs = require('fs');
var urlMD = require("url");

var COOCKIE_TOKEN = 'access-token',
    COOCKIE_EMAIL = 'email';
    COOCKIE_EXPIRY = 'token-expiry';

var queryParams = {
    '$select': 'Subject,ReceivedDateTime,From,Body',
    // '$filter':"ishouldcoco",
    // '$search':'ishouldcoco',
    // '$orderby': 'ReceivedDateTime desc',
    '$top': 20,
    '$skip': 0
};

var handle = {};
handle["/"] = home;
handle["/authorize"] = authorize;
handle["/mail"] = mail;
// handle["/calendar"] = calendar;
// handle["/contacts"] = contacts;
var codeR = '';
var _token;

app.use(express.static(__dirname + '/public'));
server.start(router.route, handle);

function home(response, request) {
    console.log("Request handler 'home' was called.");
    response.writeHead(200, {
        "Content-Type": "text/html"
    });
    if (getValueFromCookie(COOCKIE_TOKEN, request.headers.cookie) !== undefined) {
        response.write('<p>Already logged in.</p>');
        response.write('<a href="/mail">Mail</a>');
    }
    response.write('<p>Please <a href="' + authHelper.getAuthUrl() + '">sign in</a> with your Office 365 or Outlook.com account.</p>');
    // response.write('<p>Please <a href="' + authHelper.getAuthUrl() + '">sign in</a> with your Office 365 or Outlook.com account.</p>');
    response.end();
}



function authorize(response, request) {
    console.log("Request handler 'authorize' was called.");

    // The authorization code is passed as a query parameter
    var url_parts = urlMD.parse(request.url, true);
    codeR = url_parts.query.code;
    console.log("Code: " + codeR);
    authHelper.getTokenFromCode(codeR, tokenReceived, response);
}

function expired() {
    var expired_at = getValueFromCookie()
    return (Date.compare(this.token.expires_at, new Date) == -1) ? true : false
  }

function call2(error, tokn) {
    console.log("REFRESHED!!!" + tokn);
    console.log("REFRESHED!!!" + error);
    var cookies = [
        COOCKIE_TOKEN + '=' + token.token.access_token,
        COOCKIE_EMAIL + '=' + authHelper.getEmailFromIdToken(token.token.id_token)
    ];
    //expires cookie in 2033 :(

    response.setHeader('Set-Cookie', cookies);
}

function tokenReceived(response, error, token) {
    if (error) {
        console.log("Access token error: ", error.message);
        response.writeHead(200, {
            "Content-Type": "text/html"
        });
        response.write('<p>ERROR: ' + error + '</p>');
        response.end();
    } else {
        _token = token;
        console.log("TOKEN RECEIVED:::");
        console.log(token);

        var cookies = [
            COOCKIE_TOKEN + '=' + token.token.access_token + ';Max-Age=20000000000',
            COOCKIE_EMAIL + '=' + authHelper.getEmailFromIdToken(token.token.id_token) + ';Max-Age=20000000000'
        ];
        //expires cookie in 2033 :(
        try {
            response.setHeader('Set-Cookie', cookies);
            response.writeHead(302, {
                'Location': 'http://localhost:8000/mail?top=20&skip=0'
            });
            response.end();
        } catch (e) {}
    }
}

function mail(response, request) {
    var query = urlMD.parse(request.url, true).query;
    if (query.top !== undefined && query.skip !== undefined) {
        queryParams.$top = query.top;
        queryParams.$skip = query.skip;
    }
    try {
        // console.log('REFRESH: ' + _token.refresh);
        console.log('EXPIRED: ' + _token.expired);
        var expired = _token.expired();
        console.log('TOKEN EXPIRED? = ' + expired);
        if (expired) {
            // _token.refresh(call2);
            // authHelper.getTokenFromCode(codeR, tokenReceived, response);
        }

    } catch (e) {
        console.log("ERROR TOKEN OCCURRED: " + e);
    }

    var token_ac = getValueFromCookie(COOCKIE_TOKEN, request.headers.cookie);
    var email = getValueFromCookie(COOCKIE_EMAIL, request.headers.cookie);

    console.log("TOKEN RETRIEVED:::");
    //-----------------------------------------------
    if (token_ac) {
        showMail(response, request, token_ac, email);
    } else {
        response.writeHead(200, {
            "Content-Type": "text/html"
        });
        response.write('<p> No token found in cookie!</p>');
        response.end();
    }
}


function showMail(response, request, token, email) {
    response.writeHead(200, {
        "Content-Type": "text/html"
    });
    response.write('<div><h1>Your inbox</h1></div>');
    outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
    outlook.base.setAnchorMailbox(email);

    outlook.mail.getMessages({
            token: token,
            odataParams: queryParams
        },
        function(error, result) {
            if (error) {
                console.log('getMessages returned an error: ' + error);
                response.write("<p>ERROR: " + error + "</p>");
                authHelper.getTokenFromCode(codeR, tokenReceived, response);
                // response.end();
            } else if (result) {
                console.log('getMessages returned ' + result.value.length + ' messages.');
                response.write('<table><tr><th>From</th><th>Subject</th><th>Received</th><th>Allowed</th></tr>');
                result.value.forEach(function(message) {
                    // console.log('  Subject: ' + JSON.stringify(message));
                    var info = email_analyser.analyseEmail(message);
                    var from = message.From ? message.From.EmailAddress.Name : "NONE";
                    
                    var relevant = info.allow;
                    response.write(
                        '<tr><td>' + from +
                        '</td><td>' +
                        (relevant == true ? '<b>' : '') +
                        message.Subject.substring(0,50) +
                        (relevant == true ? '</b>' : '') +
                        '</td><td>' + message.ReceivedDateTime.toString() +
                        '</td><td>' + (relevant == true ? 'yes' : 'no') +
                        '</td></tr>'
                    );

                });
                response.write('</table>');
                response.end();
            }
        });
}


function getValueFromCookie(valueName, cookie) {
    if (cookie === undefined) return undefined;
    if (cookie.indexOf(valueName) !== -1) {
        var start = cookie.indexOf(valueName) + valueName.length + 1;
        var end = cookie.indexOf(';', start);
        end = end === -1 ? cookie.length : end;
        return cookie.substring(start, end);
    }
    return undefined;
}