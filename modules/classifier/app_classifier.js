var module_classifier = '';//classifier/';

var express = require('express');
var path = require('path');
var app = express();
var fs = require('fs');
var bodyParser = require('body-parser');
var corAnalyser = require('./'+module_classifier+'analysis_corpus.js');


app.use(express.static(__dirname + '/'+module_classifier+'public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
// console.log(__dirname);

app.get('/classifier', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

app.post('/classifier', function(req, res) {
    corAnalyser.analyseCorpus(req.body, sendResults, res);
});

app.listen(process.env.PORT || 3000, function() {
    console.log('CLASSIFIER WEB listening :: ', process.env.PORT || 3000);
});

function sendResults(info, res) {
	// console.log(info);
    res.send(info);
}