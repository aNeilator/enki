$(document).ready(function() {
    var subbut = $("#subbut");
    subbut.click(function() {
        if (!subbut.hasClass('loading-animation')) {
            postNow();
        }
    });
    // $("#rawText, #raw #testClasses").change(function() {
    $("#rawText").click(function() {
        console.log('clicked');
    });
    postNow();
});

function postNow() {
    $("#subbut").addClass('loading-animation');


    var corpus = '<<test>>'; //$("#check-raw").is(':checked') ? $('#rawText').val() : '';
    var corpusUrl = $('#rawUrl').val();
    var classes = $('#testClasses').val();
    var myData = {
        "corpus": corpus,
        "url": corpusUrl,
        "tag_config": {
            "total": 20,
            "shuffle": false
        },
        "threshold": 0.5
    };
    $.ajax({
        type: "POST",
        url: 'classifier/',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(myData),
        success: function(msg) {
            $("#subbut").removeClass('loading-animation');
            $("#rawUrl").removeClass('loading-animation');
            // msg = JSON.parse(msg);
            console.log(msg);
            if (msg.timeout) {
                $("#newStatus").html('Error!');
            } else if (msg.malformed) {
                $("#newStatus").html('Link 404!');
            } else {

                var words = msg.keywords;
                var html = '';
                for (key in words) {
                    // console.log(words[key]);
                    html +=
                        wordEmphasis(words[key][0], words[key][1], msg.maxfreq);;
                }
                drawClasses(msg.classifiers);
                $('#newtext').empty();
                $('#newtext').html(html);
                $('#newTitle').html(msg.mainTitle);
                $('#newSource').html(msg.mainSource);
                $('#newImage').attr("src", msg.mainImage.toString());
                $("#newStatus").html(msg.relevant ? 'ACCEPTED !' : 'REJECTED !');

            }
        }
    });

}

function drawClasses(classes) {
    var max = 0;
    for (key in classes) {
        if (max < classes[key]) max = classes[key];
    }
    $('#classesinfo').html('');
    // <div class="tabclass">Check<span class="confidence">0.99</span></div>
    var tabC = '<span class="confidence">';
    var end = '</span></div>';
    var str = '';
    for (key in classes) {
        if (classes[key] > 0 && classes[key] !== null) {
            console.log(key + " : " + classes[key] + " : " + (classes[key] < 0.2));
            var conf = classes[key].toString().substring(0, 4);
            var alpha = (conf / max) < 0.2 ? 0.2 : (conf / max);
            var tab = '<div class="tabclass" style="opacity:' + alpha + '">';

            str += tab + key + tabC + conf + end;
            $('#classesinfo').html(str);
        }
    }
    console.log('drawn');

}

function wordEmphasis(word, freq, max) {
    if (freq <= 1) return '';
    var rat = freq / max;
    rat *= 1.5;
    if (rat < 0.07) rat = 0.07;
    // console.log(word+":"+freq+" ,rat:"+rat);
    var col = 'color:inherit; opacity:' + rat;; //rgba(0,0,0,' + rat + ')';
    var str = '<span title="' + freq + '" style="' + col + ';font-weight:700;">' +
        word + ' ' + '</span>';
    return str;
}