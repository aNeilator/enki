var fs = require('fs');
var natural = require('natural');
var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var req_s = require('sync-request');
var path = require("path");
var url = require("url");

var tokenizer = new natural.WordTokenizer();
var nounInflector = new natural.NounInflector();
var verbInflector = new natural.PresentVerbInflector();
var keywordClasses = require('./classifier_config.json');
var stopwords = require('./stopwordslist').english;
var alchmy = require('./alchemy.js');

var _this = this;
var TfIdf = natural.TfIdf;

var info_template = {
    "classifiers": {},
    "keywords": {},
    "maxfreq": 0,
    "topclass": {
        "mclass": "",
        "amount": 0
    },
    "mainSource": "",
    "mainTitle": "",
    "mainImage": "",
    "malformed": false,
    "timeout": false
};

var config_template = {
    "classes": keywordClasses.keywords,
    "corpus": "",
    "url": "",
    "tag_config": {
        "total": 30,
        "shuffle": false
    },
    "threshold": 0.5
};

exports.analyseCorpus = function(body, callback, res) {

    body = validatePostJson(body, config_template);
    var info = JSON.parse(JSON.stringify(info_template));

    // LINK --------------------------------
    if (exists(body.url)) {
        transform(body, callback, res);
    } else {
        callback(processCorpus(body), res);
    }
}

function transform(body, done, res) {
    console.log('1. Parsing URL + Corpus...' + body.url);
    var sent = false;
    var timedout = false;
    var html = '';
    setTimeout(function() {
        if (!sent) {
            console.log('timeout occurred! exiting...');
            timedout = true;
            var info = {
                'timeout': true
            };
            done(info, res);
        }
    }, 20000);

    async.waterfall([
        function(cb1) {

            alchmy.keyInfo(function(underLimit, obj, err) {
                console.log(obj);
                if (underLimit) {
                    alchmy.extractText(body.url, cb1);
                } else {
                    try {
                        var html = req_s('GET', body.url).getBody();
                        html = cheerio.load(html);
                        var text = extractHTMLText(html);
                        text = getCleanText(text);
                        cb1(null, text);
                        alchmy.extractTags(text, cb1);
                    } catch (e) {
                        console.log("Malformed URL");
                        info.malformed = true;
                    }
                }
            });
            alchmy.keyInfo(
                function(underLimit, obj, err) {
                    if (underLimit) {
                        alchmy.extractText(body.url, cb1);
                    } else {
                        try {
                            var html = req_s('GET', body.url).getBody();
                            html = cheerio.load(html);
                            var text = extractHTMLText(html);
                            text = getCleanText(text);
                            cb1(null, text);
                            alchmy.extractTags(text, cb1);
                        } catch (e) {
                            var info = {
                                'malformed': true
                            };
                            done(info, res);
                        }
                    }
                });
        },
        function(text, cb2) {
            body.corpus += text;
            var info = processCorpus(body);
            try {
                html = req_s('GET', body.url).getBody();
                html = cheerio.load(html);
                info = addImageTitle(info, html, body.url);
            } catch (e) {
                console.log("Malformed URL");
                info.malformed = true;
            }
            cb2(null, info);
        },
        function(info, cb3) {
            // console.log(info);
            if (!timedout) {
                sent = true;
                cb3(info, res);
            }
        }
    ], done);
}


function generateLinkReport(data, body) {
    html = cheerio.load(data);
    var text = extractHTMLText(html);

    body.corpus = text + body.corpus;
    var info = processCorpus(body);
    info = addImageTitle(info, html, body.url);
    return info;
}

//---------------------------------------------------
exports.WordsExist = function(text, strArray) {

    if (text === undefined) return false;
    // else text = processCorpus(text);
    text = text.toLowerCase();
    var length = strArray.length;
    while (length--) {
        if (text.indexOf(strArray[length]) != -1) {
            return true;
        }
    }
    return false;
}

function parseClasses(str) {
    // str = str.trim();
    str = str.replace(/ /g, '');
    var classes = str.split(',');
    return classes;
}

function exists(obj) {
    return obj && obj !== ""; //.length>0;
}

function validatePostJson(body, template) {
    if (!exists(body.corpus)) {
        body.corpus = '';
    }
    // Classifiers ------------------------
    body.classes = template.classes;
    // Threshold -----------------
    if (!exists(body.threshold)) {
        body.threshold = template.threshold;
    }

    // Tag Config ----------------
    if (!exists(body.tag_config)) {
        body.tag_config = template.tag_config;
    }

    //--------url protocol add
    if (!/^(?:f|ht)tps?\:\/\//.test(body.url)) {
        body.url = "http://" + body.url;
    }
    return body;
}
//-------------------------HTML cheerios--------------

function addImageTitle(info, $, url) {
    $('head meta').each(function(i) {
        var meta = $(this)[0];
        if (exists(meta.attribs.property)) {
            // console.log("meta: " + meta.attribs.property);
            if (meta.attribs.property === "og:image") {
                info.mainImage = meta.attribs.content;
            }
            if (meta.attribs.property === "og:title") {
                info.mainTitle = meta.attribs.content;
            }
            if (meta.attribs.property === "og:site_name") {
                info.mainSource = meta.attribs.content;
            }
        }
    });
    if (!exists(info.mainImage)) {
        info.mainImage = extractMainImage($, extractDomain(url));
    }
    if (!exists(info.mainTitle)) {
        info.mainTitle = $('title').text();
    }
    if (!exists(info.mainSource)) {
        info.mainSource = extractDomainName(url).capitalizeFirstLetter();
    }
    return info;
}

function extractDomain(url) {
    var protocol = '';
    var domain;
    //find & remove protocol (http, ftp, etc.) and get domain
    if (url.indexOf("://") > -1) {
        var splits = url.split('/');
        domain = splits[2];
        protocol = splits[0] + splits[1] + "//";
    } else {
        domain = url.split('/')[0];
    }

    //find & remove port number
    domain = domain.split(':')[0];
    var dom = protocol + domain;
    return dom;
}

function extractDomainName(url) {
    var domain;
    if (url.indexOf("://") > -1) {
        var splits = url.split('/');
        domain = splits[2];
    } else {
        domain = url.split('/')[0];
    }
    domain = domain.split(':')[0];
    var splits = domain.split('.');
    var name = splits[splits.length - 2];
    if (name === "co") name = splits[splits.length - 3];
    return name;
}

function extractHTMLText(html) {
    var $ = html; //cheerio.load(html);
    var text = '';
    text += iterateHTMLTag($, 'body span');
    text += iterateHTMLTag($, 'body p');
    text += iterateHTMLTag($, 'body div');
    return text;
}

function iterateHTMLTag($, tag) {
    var text = '';
    $(tag).each(function(i, element) {
        var a = $(this).prev();
        text += a.text();
    });
    return text;
}

function extractMainImage($, uri) {
    var currMax = {
        'link': "",
        'width': -1
    }

    var domain = uri; //extractDomain(uri);
    $('img').each(function(i, element) {
        try {
            var image = Imager($(this)[0], domain);
            if (image.attributes.src.match('jpg') || image.attributes.src.match('jpeg')) {
                var mwidth = exists(image.attributes.width) ? image.attributes.width : 0;
                if (currMax.width <= mwidth) {
                    currMax.width = mwidth;
                    currMax.link = image.address;
                }
            }
        } catch (e) {}
    });
    return currMax.link;
}

function Imager(image, address) {
    var thiss = {};
    var at = thiss.attributes = image.attribs;

    thiss.name = path.basename(at.src, path.extname(at.src));
    thiss.saveTo = path.dirname(require.main.filename) + "/";
    thiss.extension = path.extname(at.src);
    thiss.address = url.resolve(address, at.src);
    thiss.fromAddress = address;
    return thiss;
}
//----------------------------------------------------
function getCleanText(text) {
    text = text.toLowerCase();
    var textArr = tokenizer.tokenize(text);

    // --------Stemming---------
    for (var i = textArr.length - 1; i >= 0; i--) {
        // textArr[i]= nounInflector.singularize(textArr[i]);
        // textArr[i] = verbInflector.pluralize(textArr[i]);
        if (textArr[i].length <= 2) textArr[i] = "";
    };

    // --------STOP WORDS-------
    text = '';
    for (var i = textArr.length - 1; i >= 0; i--) {
        text += textArr[i] + ' ';
    }

    for (var i = 0; i < stopwords.length; i++) {
        var re = new RegExp(' ' + stopwords[i] + ' ', 'g');
        text = text.replace(re, ' ');
    }
    return text;
}
processCorpus = function(config) {
    // text = config.corpus;
    var info = JSON.parse(JSON.stringify(info_template));

    // --------tokenise---------
    var text = getCleanText(config.corpus);

    // --------------WORD COUNT------
    var words = text.split(/\b/);
    var objCls = getClassFrequencies(words, config);

    // var tags = keyword_extractor.extract(text, {
    //     language: "english",
    //     remove_digits: true,
    //     return_changed_case: true,
    //     remove_duplicates: false,
    //     stopwords: stopwords
    // });
    // var objCls = getClassFrequencies(tags, config);
    //-------Normalise Classes in INFO-------
    info.classifiers = objCls.classifiers;
    info.maxfreq = objCls.maxfreq;
    info = normaliseClassifiers(info);
    info.keywords = objCls.keywords;

    //---is it relevant?
    info = isItRelevant(info, config.threshold);
    return info;
}

function getClassFrequencies(words, config) {
    var obj = {
        "classifiersArray": null,
        "maxfreq": 0
    };
    // --------------WORD COUNT------
    var wordCounts = new Object();
    for (var i = 0; i < words.length; i++) {
        words[i] = words[i].replace(/ /g, '');
        var wd = words[i];
        if (isNaN(wd) && /\S/.test(wd)) {
            wordCounts[words[i]] = (wordCounts[words[i]] || 0) + 1;
        }
    }

    // --------------Initialise Classes------
    var classifiersArr = {};
    for (var i = 0; i < config.classes.length; i++) {
        classifiersArr[config.classes[i]] = 0;
    }

    //----------Add Class to INFO
    var maxfreq = 0;
    for (var key in wordCounts) {
        if (key === 'length' || !wordCounts.hasOwnProperty(key)) continue;
        var freq = wordCounts[key];
        if (freq > maxfreq) maxfreq = freq;
        for (var i = 0; i < config.classes.length; i++) {
            var simConf = classifyWord(config.classes[i], key);
            classifiersArr[config.classes[i]] += simConf * freq;
            // info.classifiers[config.classes[i]] += simConf * freq;
        }
    }
    //------------SORT + top 300--------
    var sortable = [];
    for (var w in wordCounts) {
        sortable.push([w, wordCounts[w]])
    }
    sortable.sort(function(a, b) {
        return b[1] - a[1]
    });

    sortable = sortable.splice(0, config.tag_config.total);
    if (config.tag_config.shuffle === true) {
        sortable = shuffleArray(sortable);
    }
    return {
        "classifiers": classifiersArr,
        "maxfreq": maxfreq,
        "keywords": sortable
    };
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function isItRelevant(info, threshold) {
    var thresh = 6 * threshold;
    info.relevant = false;
    if (info.topclass.mclass.length > 0 && info.topclass.amount > thresh) {
        info.relevant = true;
    }
    return info;
}

function sortObj(a, b) {
    return a[1] - b[1];
}

function normaliseClassifiers(info) {
    info.topclass.mclass = "";
    info.topclass.amount = 0;
    var max = 0;
    var classes = info.classifiers;
    for (key in classes) {
        if (max < classes[key]) {
            max = classes[key];
            // console.log(key+","+classes[key]);
            info.topclass.mclass = key;
            info.topclass.amount = classes[key];
        }
    }

    for (key in classes) {
        if (info.topclass.amount > 0) {
            classes[key] = classes[key] / max;
        }
    }
    // console.log('3. Response Sent ...');
    return info;
}

function classifyWord(cls, word) {
    var thresh = 0.93;
    cls = cls.replace(/ /g, '');
    var conf = natural.JaroWinklerDistance(cls, word);
    if (conf > thresh) {
        // console.log(word + '===' + cls + ':' + conf)
        return conf;
    }
    return 0;
}

exports.extractUrls = function(html) {
    var $ = cheerio.load(html);
    var links = $('a'); //jquery get all hyperlinks
    var array = [];
    $(links).each(function(i, link) {
        var u = $(link).attr('href');
        if (u !== undefined) {
            var ignore = keywordClasses.link_reject; //['akqa', 'tel:', 'youtu'];
            if (!_this.WordsExist(u, ignore)) {
                // console.log("LINK:::" + u);
                array.push(u);
            }
        }
    });
    return array;
}

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}