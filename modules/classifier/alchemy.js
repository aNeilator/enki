var AlchemyAPI = require('alchemy-api');

var alchemy = new AlchemyAPI('983b26e666f1ed575e5973d2bff6468e56c0cfad');

exports.keyInfo = function(callback) {

    alchemy.apiKeyInfo({}, function(err, response) {
        if (!err && response.status !== 200) {
            // Do something with data
            console.log('Status:', response.status, 'Consumed:', response.consumedDailyTransactions, 'Limit:', response.dailyTransactionLimit);
            var obj = {
                'Consumed': response.consumedDailyTransactions,
                'Limit': response.dailyTransactionLimit
            }
            var consumedAll = response.consumedDailyTransactions < response.dailyTransactionLimit;
            callback(!consumedAll, obj);
        } else {
            console.log('Status:', response.status, 'Consumed:', response.consumedDailyTransactions, 'Limit:', response.dailyTransactionLimit);
            if (!err) {
                // Do something with data
                var obj = {
                    'consumed': response.consumedDailyTransactions,
                    'limit': response.dailyTransactionLimit
                }
                var underLimit = obj.consumed < obj.limit;
                console.log('Status:', response.status, 'Consumed:', response.consumedDailyTransactions, 'Limit:', response.dailyTransactionLimit);
                callback(underLimit, obj);
            } else {
                callback(false, null, err);
            }
        }
    });
}

exports.extractText = function(url, callback) {

    var options = {};
    options.sourceText = 'cleaned';
    alchemy.text(url, options, function(err, response) {
        if (err) throw err;

        // See http://www.alchemyapi.com/api/entity/htmlc.html for format of returned object
        var entities = response.text;
        // console.log(entities);
        callback(null, entities);
    });
}

exports.extractTags = function(html, callback) {
    var options = {

    };
    options.maxRetrieve = 20;
    options.keywordExtractMode = 'strict';
    options.sourceText = 'cleaned';
    // options.sentiment=1;

    alchemy.keywords(html, options, function(err, response) {
        if (err) throw err;

        // See http://www.alchemyapi.com/api/entity/htmlc.html for format of returned object
        var entities = response.keywords;
        // console.log(entities);
        callback(null, entities);
        // Do something with data
    });
}