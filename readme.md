# Love of Innovation

#### Database for the Love of Innovation Project. 

For access to the Love of Innovation project on Parse please contact Snehil Bhushan, Nick Farrant or Jack Davis. 

A description of the project can be found [here](https://extranet.akqa.com/display/AKQAPG/Love+of+Innovation)

## Go Pipeline

The Go pipeline for this project is dev-loi.akqa.technology.

#### VMs 

| Enviroment | IP          | Local DNS    | DNS                     |
|------------|-------------|--------------|-------------------------|
|  Dev       | 10.2.24.115 | vmd-loi-enki | dev.loi.akqa.technology |  
|  QA        | 10.2.30.140 | vmu-loi-enki | qa.loi.akqa.technology  |  

Dev is automatically deployed on push to master. QA must be deployed manually. 

CI scripts and keys can be found in ~/scripts and ~/keys, respectively. 

## Modules

#### 1. Classifier

#### 2. Twitter Module

The Twitter module contains a Python script, twitter.py, used to scrape Twitter for content and send it to Parse. The script uses lists of predifined [channels](parse.com/apps/love-of-innovation/collections#class/Sources).
 and [topics](parse.com/apps/love-of-innovation/collections#class/Sources) both of which are hosted by Parse.
 
Tweets are assigned tags and images based on the articles they link to. These are obtained using the Embed.ly API.  

twitter.py requires the installation of the following external dependences:

`pip install Twython`

`pip install progressbar`

#### 3. Slack Module

#### 4. Parse Module

#### 5. Discover Page

The discovery page hosts all content scraped from Twitter and submitted through Slack. To host the site locally Grunt and node are both required: 

`npm install`

`grunt dev`

`node app_discover.js`

The page will be available at http://localhost:8080.

UI testing has also been implemented using WebdriverIO and Mocha. These initial tests can be performed using a grunt task:

`grunt test`

The test requires [Chromedriver](https://code.google.com/p/selenium/wiki/ChromeDriver) to be installed locally.  

#### 6. iOS Web App

#### 7. Android Web App