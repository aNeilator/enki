cd ..

tar -xvzf sites/discover_web*.tar.gz
chmod 600 keys/LOI_dev.key

scp -o StrictHostKeyChecking=no -i keys/LOI_dev.key -r public user_1@10.2.24.115: 
scp -o StrictHostKeyChecking=no -i keys/LOI_dev.key -r modules user_1@10.2.24.115: 
scp -o StrictHostKeyChecking=no -i keys/LOI_dev.key -r package.json user_1@10.2.24.115: 
scp -o StrictHostKeyChecking=no -i keys/LOI_dev.key -r app_discover.js user_1@10.2.24.115: 

ssh -o StrictHostKeyChecking=no -i keys/LOI_dev.key user_1@10.2.24.115 'npm install' 
ssh -o StrictHostKeyChecking=no -i keys/LOI_dev.key user_1@10.2.24.115 'forever stopall' 
ssh -o StrictHostKeyChecking=no -i keys/LOI_dev.key user_1@10.2.24.115 'forever start app_discover.js' 
