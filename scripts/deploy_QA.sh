cd ..

tar -xvzf sites/discover_web*.tar.gz
chmod 600 keys/LOI_QA.key

scp -o StrictHostKeyChecking=no -i keys/LOI_QA.key -r public user_1@10.2.30.140:
scp -o StrictHostKeyChecking=no -i keys/LOI_QA.key -r modules user_1@10.2.30.140: 
scp -o StrictHostKeyChecking=no -i keys/LOI_QA.key -r package.json user_1@10.2.30.140: 
scp -o StrictHostKeyChecking=no -i keys/LOI_QA.key -r app_discover.js user_1@10.2.30.140: 

ssh -o StrictHostKeyChecking=no -i keys/LOI_QA.key user_1@10.2.30.140 'npm install' 
ssh -o StrictHostKeyChecking=no -i keys/LOI_QA.key user_1@10.2.30.140 'forever stopall' 
ssh -o StrictHostKeyChecking=no -i keys/LOI_QA.key user_1@10.2.30.140 'forever start app_discover.js' 
