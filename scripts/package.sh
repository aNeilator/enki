BASE=$PWD
TIME=`date +%s`
TAR_NAME="discover_web_$TIME.tar.gz"

cd ../modules/discover_web

npm install 
grunt dist

tar -zcvf $TAR_NAM $TAR_NAME  app_discover.js package.json modules/ public/
